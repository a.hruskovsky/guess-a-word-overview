# Guess a word overview


## Name
Guess a word overview

## Description
Project is written in React 

## Installation
- Download Node.js and install it (https://nodejs.org/en/download)
- Clone the repository (git clone https://gitlab.com/a.hruskovsky/guess-a-word-overview.git)
- Go to guess-a-word-overview directory (cd guess-a-word-overview/)
- Run npm install
- Run npm run dev

## Usage
Overview of all games and their status.
