import React, { useState, useEffect } from 'react';
import './overview.css';  // Ensure this line is here to import the styles

function Overview() {
  const [games, setGames] = useState([]);

  useEffect(() => {
    fetch('http://127.0.0.1:8080/games')
      .then(response => response.json())
      .then(data => setGames(data.games))
      .catch(err => console.error('Failed to fetch games:', err));
  }, []);

  return (
    <div className="Overview">
      <header className="Overview-header">
        <h1>Games overview</h1>
      </header>
      <div id="games">
        {games.map(game => (
          <div key={game.game_id} className="game-item">
            <div className="game-heading">Game ID: {game.game_id}</div>
            <div className="game-details">
              <p>Players: {game.players}</p>
              <p>Number of attempts: {game.attempts}</p>
              <p>Status: {game.status}</p>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default Overview;